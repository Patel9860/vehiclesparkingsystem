<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin</title>
</head>
<body>
	<h1>Admin Home Page</h1>
	<hr />
	<p align="right">
	<%
	response.setHeader("Cache-control", "no-cache");
	response.setHeader("Cache-control", "no-store");
	response.setHeader("Pragma","no-cache");
	response.setDateHeader("Expire",0);
	%>
	Hi , <%=(String) session.getAttribute("uid") %><br/>
	<a href ="admincrud" style = "text-decoration:none">Admin CRUD</a>&nbsp;&nbsp;
		<a href="signout">Logout</a>&nbsp;&nbsp;
	</p>
	<hr />
	<div align="center" >
	<table border="5">
		<tr>
			<th colspan="5">Vehicle Details</th>
		</tr>
		<tr>
			<th>Id</th>
			<th>Parking Number</th>
			<th>Vehicle Category</th>
			<th>Owner Name</th>
			<th>Mobile</th>
		</tr>
		<c:forEach items="${veh}" var="e">
			<tr style="text-align:center">
				<td>${e.getId()}</td>
				<td>${e.getParkingNumber()}</td>
				<td>${e.getVehicleCategory()}</td>
				<td>${e.getOwnerName()}</td>
				<td>${e.getMobile()}</td>
				
			</tr>
		</c:forEach>

	</table>
	</div>
</body>
</html>