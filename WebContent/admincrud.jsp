<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin CRUD</title>
</head>
<body>

<h1>Admin CRUD Operation Page</h1>
	
	<p align="right">
	<a href= "home">Home</a>&nbsp;&nbsp;
	<a href= "insert">Add New Vehicle</a>&nbsp;&nbsp;
		<a href="signout">Logout</a>&nbsp;&nbsp;
	</p>
	<hr />
	${msg}
	<div align="center" >
	<table border="5">
		<tr>
			<th colspan="7">Vehicle Details</th>
		</tr>
		<tr style="text-align: center">
			<th>Vehicle Id</th>
			<th>Parking Number</th>
			<th>Vehicle Category</th>
			<th>Owner Name</th>
			<th>Mobile</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${veh}" var="e">
			<tr style="text-align:center">
				<td>${e.getId()}</td>
				<td>${e.getParkingNumber()}</td>
				<td>${e.getVehicleCategory()}</td>
				<td>${e.getOwnerName()}</td>
				<td>${e.getMobile()}</td>
				<td><a style="text-decoration: none"
					href="update?id=${e.getId()}&ownername=${e.getOwnerName()}">Update</a></td>
				<td><a style="text-decoration: none"
					href="delete?id=${e.getId()}">Delete</a></td>
			</tr>
		</c:forEach>

	</table>
	</div>
	

</body>
</html>