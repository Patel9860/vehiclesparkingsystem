package controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IAdminDao;
import dao.IAdminDaoImpl;
import model.Admin;
import model.Vehicles;

@WebServlet(name = "AdminController", urlPatterns = { "/login", "/add", "/updateCont" })
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		IAdminDao dao = new IAdminDaoImpl();

		if (url.equals("/login")) {
			String uid = request.getParameter("uid");
			String password = request.getParameter("password");
			HttpSession session = request.getSession(true);
			session.setAttribute("uid", uid);
			System.out.println(uid + " Logged in @ " + new Date(session.getCreationTime()));

			Admin admin = new Admin(uid, password);
			int result = dao.adminLogin(admin);
			if (result > 0) {
				List<Vehicles> list = dao.viewAll();
				request.setAttribute("veh", list);
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			} else {
				request.setAttribute("error", "Hi " + uid + " , Please check your credentials");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		} else if (url.equals("/add")) {
			Integer id = Integer.parseInt(request.getParameter("id"));
			String parkingNumber = request.getParameter("parkingNumber");
			String vehicleCategory = request.getParameter("vehicleCategory");
			String ownerName = request.getParameter("ownerName");
			Long mobile = Long.parseLong(request.getParameter("mobile"));

			Vehicles vehicle = new Vehicles(id, parkingNumber, vehicleCategory, ownerName, mobile);
			int result = dao.add(vehicle);
			if (result > 0) {
				request.setAttribute("msg", id + " inserted Successfully");
			} else {
				request.setAttribute("msg", id + " duplicate Entry");
			}
			List<Vehicles> list = dao.viewAll();
			request.setAttribute("veh", list);
			request.getRequestDispatcher("admincrud.jsp").forward(request, response);
		} else if (url.contentEquals("/updateCont")) {
			int id = Integer.parseInt(request.getParameter("id"));
			String ownerName = request.getParameter("ownerName");
			Vehicles vehicle = new Vehicles();
			vehicle.setId(id);
			vehicle.setOwnerName(ownerName);
			dao.update(vehicle);
			List<Vehicles> list = dao.viewAll();
			request.setAttribute("veh", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);
		}
	}
}
