package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IAdminDao;
import dao.IAdminDaoImpl;
import model.Vehicles;

@WebServlet(name = "AdminCrudController", urlPatterns = { "/admincrud", "/home", "/delete", "/insert", "/signout",
		"/update" })
public class AdminCrudController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getServletPath();
		IAdminDao dao = new IAdminDaoImpl();
		Vehicles vehicle = new Vehicles();

		if (url.contentEquals("/admincrud")) {

			List<Vehicles> list = dao.viewAll();
			request.setAttribute("veh", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);
		} else if (url.equals("/home")) {
			List<Vehicles> list = dao.viewAll();
			request.setAttribute("veh", list);
			request.getRequestDispatcher("admin.jsp").forward(request, response);
		}

		else if (url.equals("/delete")) {

			int id = Integer.parseInt(request.getParameter("id"));
			vehicle.setId(id);
			dao.delete(vehicle);
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.print(id + " deleted Successfully<br/>");
			List<Vehicles> list = dao.viewAll();
			request.setAttribute("veh", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);
			// request.getRequestDispatcher("admincrud").include(request, response);//dont
			// redirect
		}

		else if (url.equals("/insert")) {
			request.getRequestDispatcher("insert.jsp").forward(request, response);
		}

		else if (url.equals("/signout")) {
			HttpSession session = request.getSession(false);
			String uid = (String) session.getAttribute("uid");
			System.out.println(uid + " logged out @ " + new Date());
			session.invalidate();
			request.setAttribute("uid", uid + " Logged out successfully!!!");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}

		else if (url.equals("/update")) {
			int id = Integer.parseInt(request.getParameter("id"));
			String ownerName = request.getParameter("ownerName");

			vehicle.setId(id);
			vehicle.setOwnerName(ownerName);
			request.setAttribute("veh", vehicle);
			request.getRequestDispatcher("update.jsp").forward(request, response);
		}
	}
}
