package dao;

import java.util.List;

import model.Admin;
import model.Vehicles;

public interface IAdminDao {

	public int adminLogin(Admin admin);

	public List<Vehicles> viewAll();
	
	public void delete(Vehicles vehicle);
	
	public void update(Vehicles vehicle);
	
	public int add(Vehicles vehicle);
}
