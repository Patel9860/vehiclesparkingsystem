package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.Vehicles;
import util.DbUtil;
import util.QueryUtil;

public class IAdminDaoImpl implements IAdminDao {

	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int adminLogin(Admin admin) {
		result = 0;
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.adminLogin);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Exception occures in adminAuthentication");
		}
		return result;
	}

	@Override
	public List<Vehicles> viewAll() {
		List<Vehicles> list = new ArrayList<Vehicles>();
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.viewALL);
			rs = pst.executeQuery();
			while (rs.next()) {
				Vehicles vehicle = new Vehicles(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getLong(5));
				list.add(vehicle);
			}
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Exception occures in View All");
		}
		return list;
	}

	@Override
	public void delete(Vehicles vehicle) {
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.delete);
			pst.setInt(1, vehicle.getId());
			pst.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {

		}

	}

	@Override
	public void update(Vehicles vehicle) {
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.update);
			pst.setString(1, vehicle.getOwnerName());
			pst.setInt(2, vehicle.getId());
			pst.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {

		}

	}

	@Override
	public int add(Vehicles vehicle) {
		result = 0;
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.add);
			pst.setInt(1, vehicle.getId());
			pst.setString(2, vehicle.getParkingNumber());
			pst.setString(3, vehicle.getVehicleCategory());
			pst.setString(4, vehicle.getOwnerName());
			pst.setLong(5, vehicle.getMobile());
			result = pst.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			return result;
		}
		return result;
	}

}
