package model;

public class Vehicles {

	private Integer id;
	 private String parkingNumber;
	 private String vehicleCategory;
	 private String ownerName;
	 private Long mobile;
	 
	 public Vehicles() {
		// TODO Auto-generated constructor stub
	}

	public Vehicles(Integer id, String parkingNumber, String vehicleCategory, String ownerName, Long mobile) {
		super();
		this.id = id;
		this.parkingNumber = parkingNumber;
		this.vehicleCategory = vehicleCategory;
		this.ownerName = ownerName;
		this.mobile = mobile;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getParkingNumber() {
		return parkingNumber;
	}

	public void setParkingNumber(String parkingNumber) {
		this.parkingNumber = parkingNumber;
	}

	public String getVehicleCategory() {
		return vehicleCategory;
	}

	public void setVehicleCategory(String vehicleCategory) {
		this.vehicleCategory = vehicleCategory;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}
	 
	 
}

